extern crate csv;
extern crate nyx_space as nyx;
extern crate rayon;
extern crate serde_json;
use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;

use nyx::dynamics::{Harmonics, OrbitalDynamics};
use nyx::md::ui::Traj;
use nyx::od::ui::GroundStation;
use nyx::propagators::Propagator;
use nyx::time::{Epoch, TimeUnitHelper};
use nyx::Orbit;
use nyx::{cosmic::Cosm, io::gravity::HarmonicsMem};

use crate::cities::{Constellation, WorldCity};
use std::time::Instant;

use rayon::prelude::*;

mod cities;

const MAX_CITIES: usize = 100;
const EXPORT_TRAJ: bool = true; // Export the trajectories to plot with plot_traj of nyx_plotly
const EXPORT_GROUNDTRACK: bool = false; // Export the ground track to plot with plot_ground_track.py of nyx_plotly

fn main() {
    // let buf = std::fs::read("data/worldcities.csv").unwrap();

    // Load the cities
    let mut rdr = csv::Reader::from_path("data/worldcities.csv").unwrap();
    let mut cities = Vec::with_capacity(MAX_CITIES);
    for result in rdr.deserialize() {
        // Notice that we need to provide a type hint for automatic
        // deserialization.
        let record: WorldCity = result.unwrap();
        cities.push(record);
        if cities.len() == MAX_CITIES {
            break;
        }
    }

    println!("Read in {} cities", cities.len());

    // Load the constellation

    // Parse the string of data into serde_json::Value.
    let file = File::open("data/constellation.json").unwrap();
    let reader = BufReader::new(file);

    // Read the JSON contents of the file as an instance of `User`.
    let u: Constellation = serde_json::from_reader(reader).unwrap();

    // Convert the launches and payloads into orbits
    let mut orbits = Vec::with_capacity(400);

    let cosm = Cosm::de438();
    let eme2k = cosm.frame("EME2000");
    let iau_earth = cosm.frame("IAU Earth");
    let dt = Epoch::from_gregorian_tai_at_midnight(2021, 8, 12);

    for launch in &u.launches {
        for payload in &launch.payload {
            let this_orbit = Orbit::keplerian(
                launch.orbit.a,
                launch.orbit.e,
                launch.orbit.i.to_degrees(),
                launch.orbit.Om.to_degrees(),
                launch.orbit.om.to_degrees(),
                payload.f,
                dt,
                eme2k,
            );
            orbits.push(this_orbit);
        }
    }

    // Set up the dynamics
    let monte_earth_j2 = -0.000_484_169_325_971;
    let earth_sph_harm = HarmonicsMem::from_j2(monte_earth_j2);
    let harmonics = Harmonics::from_stor(iau_earth, earth_sph_harm, cosm.clone());

    let dynamics = OrbitalDynamics::from_model(harmonics);

    let setup = Propagator::default(dynamics);

    let start = Instant::now();
    let trajs = orbits
        .par_iter()
        .map(|orbit| {
            let (_, traj) = setup.with(*orbit).for_duration_with_traj(1.days()).unwrap();
            traj
        })
        .collect::<Vec<Traj<Orbit>>>();

    println!(
        "Done propagation {} in {} seconds",
        trajs.len(),
        (Instant::now() - start).as_secs_f64()
    );

    if EXPORT_GROUNDTRACK {
        let start = Instant::now();
        trajs.iter().enumerate().par_bridge().for_each_with(
            cosm.clone(),
            |cosm, (traj_no, traj)| {
                traj.to_groundtrack_csv(
                    &format!("coverage-traj-{}.csv", traj_no),
                    cosm.frame("IAU Earth"),
                    cosm.clone(),
                )
                .unwrap();
            },
        );
        println!(
            "Exported ground tracks in {} seconds",
            (Instant::now() - start).as_secs_f64()
        );
    }

    if EXPORT_TRAJ {
        let start = Instant::now();
        trajs.iter().enumerate().par_bridge().for_each_with(
            cosm.clone(),
            |cosm, (traj_no, traj)| {
                traj.to_csv(&format!("earth-traj-{}.csv", traj_no), cosm.clone())
                    .unwrap();
            },
        );
        println!(
            "Exported trajectories in {} seconds",
            (Instant::now() - start).as_secs_f64()
        );
    }

    let start = Instant::now();
    // For every city, every 30 seconds, how many spacecraft are in line of light?
    // If elevation > 15.0 degrees, any azimuth
    cities.par_iter().for_each(|city| {
        let gs = GroundStation::from_point(
            city.city.clone(),
            city.lat,
            city.lng,
            0.0,
            iau_earth,
            cosm.clone(),
        );
        let mut counts = HashMap::<usize, u32>::new();
        for traj in &trajs {
            let mut offset = 0;
            for state in traj.every(30.seconds()) {
                let (el, _, _) = gs.elevation_of(&state);
                if el >= 15.0 {
                    if let Some(val) = counts.get_mut(&offset) {
                        *val += 1;
                    } else {
                        counts.insert(offset, 1);
                    };
                }
                offset += 1;
            }
        }
        println!("{}\n{:?}\n\n", city.city, counts);
    });

    println!(
        "Done with all {} cities in {} seconds",
        cities.len(),
        (Instant::now() - start).as_secs_f64()
    );
}
