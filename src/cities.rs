extern crate csv;
extern crate serde_derive;

use serde_derive::Deserialize;

#[derive(Debug, Deserialize)]
pub struct WorldCity {
    pub city: String,
    pub lat: f64,
    pub lng: f64,
    pub population: Option<f64>,
}

#[derive(Debug, Deserialize)]
pub struct Constellation {
    pub launches: Vec<Launch>,
}

#[derive(Debug, Deserialize)]
pub struct Launch {
    pub launchName: String,
    pub orbit: OrbitJson,
    pub payload: Vec<Payload>,
}

#[derive(Debug, Deserialize)]
pub struct OrbitJson {
    pub a: f64,
    pub e: f64,
    pub i: f64,
    pub Om: f64,
    pub om: f64,
}

#[derive(Debug, Deserialize)]
pub struct Payload {
    pub name: String,
    pub f: f64,
}
