# Coverage

A quick demo of a coverage analysis where the spacecraft are defined in a json file. This code simply propagates (in parallel) each of the 250 spacecraft for one day (Earth-centered two body with J2). Then, every 30 seconds in each of the trajectories, it will count how many spacecraft each of the cities can see.

Note that for quick assessment, the 41,100 cities file is trimmed at runtime to only read the first 100 cities.

## Performance
1. Propagation of the 250 spacecraft for one day take about 5.5 seconds.
1. Perform the O(n^2) search through N cities takes another 139 to 141 seconds _if an RK89 with trajectories_ is used. Using a static allocation is actually slightly longer in time (147 seconds)!

This code heavily relies on safe multithreading in Rust: first, each spacecraft propagation happens on its own thread; then each city is searched on its own thread. These threads are executed on a thread pool to avoid overwhelming the computer.